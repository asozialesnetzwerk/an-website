name: Release
on:
  schedule:
    - cron: "0 0 * * *"
  push:
    tags: [ v* ]
env:
  PIP_DISABLE_PIP_VERSION_CHECK: yes

jobs:

  build:
    name: Build
    runs-on: ubuntu-latest
    timeout-minutes: 10
    steps:
      - name: Checkout repository
        uses: actions/checkout@v3
      - name: Set up Python
        uses: actions/setup-python@v3
        with:
          python-version: "3.10"
      - name: Fetch tags
        if: github.event_name == 'schedule'
        run: git fetch --tags
      - name: Check if already released
        if: github.event_name == 'schedule'
        run: test -z $(git tag --list "v*" --points-at)
      - name: Create tag
        if: github.event_name == 'schedule'
        run: git tag $(python -c "from datetime import datetime; print(datetime.utcnow().strftime('v%y.%m.%d'))")
      - name: Push tag
        if: github.event_name == 'schedule'
        run: git push --tags
      - name: Install build frontend
        run: pip install "build>=0.8,<2"
      - name: Build
        run: python -m build
      - name: Upload as build artifact
        uses: actions/upload-artifact@v3
        with:
          path: dist

  tests:
    name: Run tests
    runs-on: ubuntu-latest
    timeout-minutes: 10
    steps:
      - name: Checkout repository
        uses: actions/checkout@v3
      - name: Set up Python
        uses: actions/setup-python@v3
        with:
          python-version: "3.10"
          cache: "pip"
          cache-dependency-path: requirements*.txt
      - name: Install libcurl4-openssl-dev for compiling PycURL
        run: |
          sudo apt-get update
          sudo apt-get install -y libcurl4-openssl-dev
      - name: Install requirements
        run: pip install -r requirements.txt
      - name: Install pytest, coverage and html5lib (needed for the tests)
        run: grep -E "^(pytest|coverage|html5lib)" requirements-dev.txt | xargs pip install
      - name: Run pytest
        run: pytest --durations=0 --verbose --cov=an_website --cov-config=.coveragerc tests
      - name: Upload coverage as artifact
        uses: actions/upload-artifact@v3
        with:
          path: .coverage
          name: coverage

  hashes:
    name: Hash files
    runs-on: ubuntu-latest
    timeout-minutes: 10
    steps:
      - name: Checkout repository
        uses: actions/checkout@v3
      - name: Set up Python
        uses: actions/setup-python@v3
        with:
          python-version: "3.10"
      - name: Hash files
        run: ./hash_files.py > hashes
      - name: Upload as build artifact
        uses: actions/upload-artifact@v3
        with:
          path: hashes
          name: hashes

  release:
    name: Create release
    runs-on: ubuntu-latest
    timeout-minutes: 10
    needs:
      - build
      - tests
    steps:
      - name: Download artifact
        uses: actions/download-artifact@v3
        with:
          name: artifact
      - name: Set up Python
        uses: actions/setup-python@v3
        with:
          python-version: "3.10"
      - name: Use black magic
        id: version
        shell: python
        run: |
          from datetime import datetime
          if "${{ github.event_name }}" == "schedule":
            print(f"::set-output name=TAG_NAME::{datetime.utcnow().strftime('v%y.%m.%d')}")
          elif "${{ github.event_name }}" == "push" and "${{ github.ref_type }}" == "tag":
            print("::set-output name=TAG_NAME::${{ github.ref_name }}")
      - name: Create release
        uses: softprops/action-gh-release@v1
        with:
          tag_name: ${{ steps.version.outputs.TAG_NAME }}
          generate_release_notes: true
          files: |
            *.tar.gz
            *.whl

  pypi:
    name: Upload to PyPI
    runs-on: ubuntu-latest
    timeout-minutes: 10
    needs:
      - build
      - tests
    steps:
      - name: Download artifact
        uses: actions/download-artifact@v3
        with:
          name: artifact
      - name: Set up Python
        uses: actions/setup-python@v3
        with:
          python-version: "3.10"
      - name: Install Twine
        run: pip install "twine~=4.0"
      - name: Run Twine
        run: twine upload --verbose --disable-progress-bar *.tar.gz *.whl
        env:
          TWINE_USERNAME: __token__
          TWINE_PASSWORD: ${{ secrets.PYPI_TOKEN }}

  deploy:
    name: Deploy website
    runs-on: ubuntu-latest
    timeout-minutes: 10
    if: github.event_name == 'schedule'
    needs:
      - build
      - tests
    steps:
      - name: Download artifact
        uses: actions/download-artifact@v3
        with:
          name: artifact
      - name: Deploy website
        run: |
          curl -s -S -T *.whl https://asozial.org/api/update/ \
            -H "Authorization: ${{ secrets.UPDATE_API_SECRET }}"

  gh_pages:
    name: GitHub Pages
    runs-on: ubuntu-latest
    timeout-minutes: 10
    if: github.event_name == 'schedule'
    needs:
      - deploy
      - hashes
    steps:
      - name: Checkout repository
        uses: actions/checkout@v3
      - name: Set up Python
        uses: actions/setup-python@v3
        with:
          python-version: "3.10"
          cache: "pip"
          cache-dependency-path: requirements*.txt
      - name: Download artifacts
        uses: actions/download-artifact@v3
      - name: Move files
        run: |
          mkdir public
          mv hashes/hashes public/hashes.txt
          mv coverage/.coverage .
      - name: Create coverage files
        run: |
          grep "^coverage" requirements-dev.txt | xargs pip install
          coverage report --precision=3 --sort=cover --skip-covered
          coverage html -d "public/coverage"
          coverage json -o "public/coverage.json"
          set -x
          ./generate-badge.sh > public/coverage/badge.svg
          rm public/coverage/.gitignore
      - name: Push hashes and coverage to GitHub Pages
        uses: peaceiris/actions-gh-pages@v3
        with:
          github_token: ${{ secrets.GITHUB_TOKEN }}
          force_orphan: true
          commit_message: ${{ github.event.head_commit.message }}
